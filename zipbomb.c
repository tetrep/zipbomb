#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

#define TWO_GB 2048000000
#define FOUR_GB 4096000000
#define SIX_GB 6144000000
#define EIGHT_GB 8192000000
#define TEN_GB 10240000000

#define CHUNK_SIZE 1024

void gtfo (char *msg, int exit_code) {
  fprintf(stderr, "%s\n", msg);
  exit(exit_code);
}

void gen_characters(int fd, char c, int chunk_size, long num_char) {
  char *chunk;

  if (NULL == (chunk = malloc(chunk_size))) {
    gtfo("[Error]: could not allocate memory for chunk", -1);
  }
  memset(chunk, c, chunk_size);

  if (0 != num_char % chunk_size) {
    fprintf(stderr, "[WARNING]: chunk size does not evenly divide number of characters to generate");
  }

  for (; 0 < num_char; num_char -= chunk_size) {
    write(fd, chunk, chunk_size);
  }
}

int main () {
  gen_characters(1, 'A', CHUNK_SIZE, TWO_GB);

  return 0;
}
